#!/bin/bash
fsmunion E0.fsm I1.fsm >E0vI1.fsm
fsmconcat X3.fsm E0vI1.fsm >3x.fsm
fsmconcat E2.fsm ../../romano_arabe/transdutor_unidades/Transdutor/tU.fsm >E2U.fsm
fsmunion E2U.fsm 3x.fsm >E2Uv3x.fsm
fsmconcat XE.fsm E2Uv3x.fsm >2x3x.fsm
fsmconcat E1.fsm ../../romano_arabe/transdutor_unidades/Transdutor/tU.fsm >E1U.fsm
fsmunion E1U.fsm 2x3x.fsm >E1Uv2x3x.fsm
fsmconcat XE.fsm E1Uv2x3x.fsm >1x2x3x.fsm
fsmconcat E0.fsm ../../romano_arabe/transdutor_unidades/Transdutor/tU.fsm >E0U.fsm
fsmunion E0U.fsm 1x2x3x.fsm >dias.fsm
fsmdraw -i ../data.syms -o ../data.syms <dias.fsm | dot -Tpdf > dias.pdf
