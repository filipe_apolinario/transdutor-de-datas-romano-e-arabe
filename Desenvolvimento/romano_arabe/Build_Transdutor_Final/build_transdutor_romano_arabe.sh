#!/bin/bash

#	Fazer isto para criar os E's

fsmcompile -t -i ../transdutores_auxiliares/transdutor_unidades/Transdutor/data.syms -o ../transdutores_auxiliares/transdutor_unidades/Transdutor/data.syms <../transdutores_auxiliares/E1.txt >E1.fsm
fsmcompile -t -i ../transdutores_auxiliares/transdutor_unidades/Transdutor/data.syms -o ../transdutores_auxiliares/transdutor_unidades/Transdutor/data.syms <../transdutores_auxiliares/E2.txt >E2.fsm
fsmcompile -t -i ../transdutores_auxiliares/transdutor_unidades/Transdutor/data.syms -o ../transdutores_auxiliares/transdutor_unidades/Transdutor/data.syms <../transdutores_auxiliares/E3.txt >E3.fsm


#fsmdraw -i ../transdutores_auxiliares/transdutor_unidades/Transdutor/data.syms -o ../transdutores_auxiliares/transdutor_unidades/Transdutor/data.syms <E1.fsm | dot -Tpdf > E1.pdf

#	Dezenas:

fsmconcat ../transdutores_auxiliares/transdutor_dezenas/Transdutor/tD.fsm ../transdutores_auxiliares/transdutor_unidades/Transdutor/tU.fsm >DU.fsm

fsmconcat ../transdutores_auxiliares/transdutor_dezenas/Transdutor/tD.fsm E1.fsm >D0.fsm

fsmunion D0.fsm DU.fsm >DUvD0.fsm

fsmdraw -i ../transdutores_auxiliares/transdutor_unidades/Transdutor/data.syms -o ../transdutores_auxiliares/transdutor_unidades/Transdutor/data.syms <DUvD0.fsm | dot -Tpdf > DUvD0.pdf

#	Centenas:

fsmconcat E1.fsm ../transdutores_auxiliares/transdutor_unidades/Transdutor/tU.fsm >0U.fsm

#fsmconcat ../transdutores_auxiliares/transdutor_centenas/Transdutor/tC.fsm DU

fsmconcat ../transdutores_auxiliares/transdutor_centenas/Transdutor/tC.fsm DUvD0.fsm >CD.fsm

fsmconcat ../transdutores_auxiliares/transdutor_centenas/Transdutor/tC.fsm 0U.fsm >C0U.fsm

fsmcompile -t -i ../transdutores_auxiliares/transdutor_unidades/Transdutor/data.syms -o ../transdutores_auxiliares/transdutor_unidades/Transdutor/data.syms <../transdutores_auxiliares/E2.txt >E2.fsm

fsmconcat ../transdutores_auxiliares/transdutor_centenas/Transdutor/tC.fsm E2.fsm >C00.fsm

fsmunion CD.fsm C00.fsm >CD\ e\ C00.fsm

fsmunion CD.fsm C00.fsm >CDeC00.fsm

 fsmunion CD.fsm C00.fsm >CDUC00.fsm

fsmunion CDUC00.fsm C0U.fsm >centenas.fsm

fsmdraw -i ../transdutores_auxiliares/transdutor_unidades/Transdutor/data.syms -o ../transdutores_auxiliares/transdutor_unidades/Transdutor/data.syms <centenas.fsm | dot -Tpdf > centenas.pdf



#	Milhares:

fsmconcat E1.fsm DUvD0.fsm >0D.fsm
fsmconcat E2.fsm ../transdutores_auxiliares/transdutor_unidades/Transdutor/tU.fsm >00U.fsm 

fsmconcat ../transdutores_auxiliares/transdutor_milhares/Transdutor/tM.fsm centenas.fsm >MC.fsm
fsmconcat ../transdutores_auxiliares/transdutor_milhares/Transdutor/tM.fsm 0D.fsm >M0D.fsm
fsmconcat ../transdutores_auxiliares/transdutor_milhares/Transdutor/tM.fsm 00U.fsm >M00U.fsm
fsmconcat ../transdutores_auxiliares/transdutor_milhares/Transdutor/tM.fsm E3.fsm >M000.fsm

fsmunion MC.fsm M0D.fsm >MCM0D.fsm
fsmunion MCM0D.fsm M00U.fsm >MCM0DM00U.fsm
fsmunion MCM0DM00U.fsm M000.fsm >milhares.fsm
fsmdraw -i ../transdutores_auxiliares/transdutor_unidades/Transdutor/data.syms -o ../transdutores_auxiliares/transdutor_unidades/Transdutor/data.syms <milhares.fsm | dot -Tpdf > milhares.pdf


#	Transdutor final

fsmunion ../transdutores_auxiliares/transdutor_unidades/Transdutor/tU.fsm DUvD0.fsm >UvD.fsm
fsmunion UvD.fsm centenas.fsm >UvDvC.fsm
fsmunion UvDvC.fsm milhares.fsm >romanoArabe.fsm
fsmdraw -i ../transdutores_auxiliares/transdutor_unidades/Transdutor/data.syms -o ../transdutores_auxiliares/transdutor_unidades/Transdutor/data.syms <romanoArabe.fsm | dot -Tpdf > romanoArabe.pdf
mv romanoArabe.fsm romanoArabe.pdf ../Transdutor_Romano_Arabe/Transdutor/
mv centenas.fsm DUvD0.fsm milhares.fsm ../
rm *.fsm
mv ../centenas.fsm ../DUvD0.fsm ../milhares.fsm .


