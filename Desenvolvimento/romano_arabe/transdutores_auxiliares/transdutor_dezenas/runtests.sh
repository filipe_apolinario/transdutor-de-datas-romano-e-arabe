#!/bin/bash

COUNTER=0
cd testes/in/
for x in *; do
	echo $x;	
	cd ..
	mkdir out/$x/
	fsmcompose in/$x/$x.fsm ../Transdutor/tD.fsm > out/$x/$x.fsm
	fsmdraw -i ../Transdutor/data.syms -o ../Transdutor/data.syms < out/$x/$x.fsm | dot -Tpdf > out/$x/$x.pdf
	COUNTER=$((COUNTER+1))
	cd in
done
echo $COUNTER ;
