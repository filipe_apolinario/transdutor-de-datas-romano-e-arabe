#!/bin/bash

fsmcompile -t -i Transdutor/data.syms -o Transdutor/data.syms <E0.txt >build/E0.fsm
fsminvert ../romano_arabe/Transdutor_Romano_Arabe/Transdutor/romanoArabe.fsm >build/arabe_romano.fsm
fsmconcat build/E0.fsm build/arabe_romano.fsm >Transdutor/arabe_romano.fsm
fsmdraw -i Transdutor/data.syms -o Transdutor/data.syms <Transdutor/arabe_romano.fsm | dot -Tpdf >Transdutor/arabe_romano.pdf
rm build/arabe_romano.fsm
