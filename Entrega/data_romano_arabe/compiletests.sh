#!/bin/bash

COUNTER=0
cd testes/teststxt/
for x in *.txt; do
	echo $x;	
	cd ..
	mkdir in/${x%.txt}
	fsmcompile -t -i ../../data.syms -o ../../data.syms <teststxt/$x >in/${x%.txt}/${x%.txt}.fsm
	fsmdraw -i ../../data.syms -o ../../data.syms <in/${x%.txt}/${x%.txt}.fsm | dot -Tpdf > in/${x%.txt}/${x%.txt}.pdf
COUNTER=$((COUNTER+1))
cd teststxt/

done

echo $COUNTER ;
