printf '\t...a compilar transdutores auxiliares\n'
fsmcompile -t -i ../data.syms -o ../data.syms <transdutores\ auxiliares/slash.txt >transdutores\ auxiliares/slash.fsm
fsmdraw -i ../data.syms -o ../data.syms < transdutores\ auxiliares/slash.fsm | dot -Tpdf > transdutores\ auxiliares/slash.pdf
cd transdutores\ auxiliares/dias/
sh dias.sh
cd ../../
cd transdutores\ auxiliares/meses/
sh meses.sh
cd ../../


printf '\t...a gerar transdutor final\n'
fsmconcat transdutores\ auxiliares/slash.fsm ../romano_arabe/romanoArabe.fsm >transdutores\ auxiliares/sAno.fsm


fsmconcat transdutores\ auxiliares/meses/meses.fsm transdutores\ auxiliares/sAno.fsm >transdutores\ auxiliares/MesessAno.fsm

fsmconcat transdutores\ auxiliares/slash.fsm transdutores\ auxiliares/MesessAno.fsm >transdutores\ auxiliares/sMesessAno.fsm

fsmconcat transdutores\ auxiliares/dias/dias.fsm transdutores\ auxiliares/sMesessAno.fsm >data_romano_arabe.fsm

printf '\ttransdutor de datas romano_arabe criado\n'
fsmdraw -i ../data.syms -o ../data.syms < data_romano_arabe.fsm | dot -Tpdf > data_romano_arabe.pdf
rm transdutores\ auxiliares/*.fsm transdutores\ auxiliares/dias/*.fsm transdutores\ auxiliares/meses/*.fsm

