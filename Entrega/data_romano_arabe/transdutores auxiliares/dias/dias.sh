#!/bin/bash
fsmcompile -t -i ../../../data.syms -o ../../../data.syms <E0.txt >E0.fsm
fsmcompile -t -i ../../../data.syms -o ../../../data.syms <E1.txt >E1.fsm
fsmcompile -t -i ../../../data.syms -o ../../../data.syms <E2.txt >E2.fsm
fsmcompile -t -i ../../../data.syms -o ../../../data.syms <I1.txt >I1.fsm
fsmcompile -t -i ../../../data.syms -o ../../../data.syms <X3.txt >X3.fsm
fsmcompile -t -i ../../../data.syms -o ../../../data.syms <XE.txt >XE.fsm
fsmdraw -i ../../../data.syms -o ../../../data.syms <E0.fsm | dot -Tpdf > E0.pdf
fsmdraw -i ../../../data.syms -o ../../../data.syms <E1.fsm | dot -Tpdf > E1.pdf
fsmdraw -i ../../../data.syms -o ../../../data.syms <E2.fsm | dot -Tpdf > E2.pdf
fsmdraw -i ../../../data.syms -o ../../../data.syms <I1.fsm | dot -Tpdf > I1.pdf
fsmdraw -i ../../../data.syms -o ../../../data.syms <X3.fsm | dot -Tpdf > X3.pdf
fsmdraw -i ../../../data.syms -o ../../../data.syms <XE.fsm | dot -Tpdf > XE.pdf


fsmunion E0.fsm I1.fsm >E0vI1.fsm
fsmconcat X3.fsm E0vI1.fsm >3x.fsm
fsmconcat E2.fsm ../../../romano_arabe/transdutores_auxiliares/tU.fsm >E2U.fsm
fsmunion E2U.fsm 3x.fsm >E2Uv3x.fsm
fsmconcat XE.fsm E2Uv3x.fsm >2x3x.fsm
fsmconcat E1.fsm ../../../romano_arabe/transdutores_auxiliares/tU.fsm >E1U.fsm
fsmunion E1U.fsm 2x3x.fsm >E1Uv2x3x.fsm
fsmconcat XE.fsm E1Uv2x3x.fsm >1x2x3x.fsm
fsmconcat E0.fsm ../../../romano_arabe/transdutores_auxiliares/tU.fsm >E0U.fsm
fsmunion E0U.fsm 1x2x3x.fsm >dias.fsm
fsmdraw -i ../../../data.syms -o ../../../data.syms <dias.fsm | dot -Tpdf > dias.pdf
