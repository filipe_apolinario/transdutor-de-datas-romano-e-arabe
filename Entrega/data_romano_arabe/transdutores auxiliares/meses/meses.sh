fsmcompile -t -i ../../../data.syms -o ../../../data.syms <I2.txt >I2.fsm
fsmcompile -t -i ../../../data.syms -o ../../../data.syms <IE.txt >IE.fsm
fsmcompile -t -i ../../../data.syms -o ../../../data.syms <X1.txt >X1.fsm
fsmcompile -t -i ../../../data.syms -o ../../../data.syms <E1.txt >E1.fsm
fsmcompile -t -i ../../../data.syms -o ../../../data.syms <E0.txt >E0.fsm
fsmdraw -i ../../../data.syms -o ../../../data.syms <I2.fsm | dot -Tpdf > I2.pdf
fsmdraw -i ../../../data.syms -o ../../../data.syms <IE.fsm | dot -Tpdf > IE.pdf
fsmdraw -i ../../../data.syms -o ../../../data.syms <X1.fsm | dot -Tpdf > X1.pdf
fsmdraw -i ../../../data.syms -o ../../../data.syms <E1.fsm | dot -Tpdf > E1.pdf
fsmdraw -i ../../../data.syms -o ../../../data.syms <E0.fsm | dot -Tpdf > E0.pdf


fsmunion E1.fsm I2.fsm >E1vI2.fsm
fsmconcat IE.fsm E1vI2.fsm  >I2I.fsm

 fsmunion E0.fsm I2I.fsm >E0vI2I.fsm
fsmconcat X1.fsm E0vI2I.fsm  >1x1i2i.fsm
fsmconcat E0.fsm ../../../romano_arabe/transdutores_auxiliares/tU.fsm  >E0U.fsm
fsmunion E0U.fsm 1x1i2i.fsm >meses.fsm
fsmdraw -i ../../../data.syms -o ../../../data.syms <meses.fsm | dot -Tpdf > meses.pdf

