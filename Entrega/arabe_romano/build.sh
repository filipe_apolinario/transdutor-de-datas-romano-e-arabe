#!/bin/bash

printf '\t...a compilar o transdutor auxiliar\n'

fsmcompile -t -i ../data.syms -o ../data.syms <transdutores\ aux/E0.txt >transdutores\ aux/E0.fsm
fsmdraw -i ../data.syms -o ../data.syms <transdutores\ aux/E0.fsm | dot -Tpdf >transdutores\ aux/E0.pdf

printf '\t...a gerar o transdutor final\n'
fsminvert ../romano_arabe/romanoArabe.fsm >transdutores\ aux/a_r.fsm
fsmconcat transdutores\ aux/E0.fsm transdutores\ aux/a_r.fsm >arabe_romano.fsm

printf '\t transdutor arabe_comano criado\n'
fsmdraw -i ../data.syms -o ../data.syms <arabe_romano.fsm | dot -Tpdf >arabe_romano.pdf
rm transdutores\ aux/*.fsm
