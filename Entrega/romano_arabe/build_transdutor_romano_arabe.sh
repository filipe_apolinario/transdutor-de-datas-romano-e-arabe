#!/bin/bash
#compilar transdutores auxiliares
printf "\t...a gerar transdutores auxiliares\n" 
fsmcompile -t -i ../data.syms -o ../data.syms <transdutores_auxiliares/tD.txt >transdutores_auxiliares/tD.fsm
fsmcompile -t -i ../data.syms -o ../data.syms <transdutores_auxiliares/tC.txt >transdutores_auxiliares/tC.fsm
fsmcompile -t -i ../data.syms -o ../data.syms <transdutores_auxiliares/tM.txt >transdutores_auxiliares/tM.fsm
fsmcompile -t -i ../data.syms -o ../data.syms <transdutores_auxiliares/tU.txt >transdutores_auxiliares/tU.fsm

fsmdraw -i ../data.syms -o ../data.syms <transdutores_auxiliares/tU.fsm | dot -Tpdf > transdutores_auxiliares/tU.pdf
fsmdraw -i ../data.syms -o ../data.syms <transdutores_auxiliares/tD.fsm | dot -Tpdf > transdutores_auxiliares/tD.pdf
fsmdraw -i ../data.syms -o ../data.syms <transdutores_auxiliares/tC.fsm | dot -Tpdf > transdutores_auxiliares/tC.pdf
fsmdraw -i ../data.syms -o ../data.syms <transdutores_auxiliares/tM.fsm | dot -Tpdf > transdutores_auxiliares/tM.pdf

#	Fazer isto para criar os E's

fsmcompile -t -i ../data.syms -o ../data.syms <transdutores_auxiliares/E1.txt >transdutores_auxiliares/build/E1.fsm
fsmcompile -t -i ../data.syms -o ../data.syms <transdutores_auxiliares/E2.txt >transdutores_auxiliares/build/E2.fsm
fsmcompile -t -i ../data.syms -o ../data.syms <transdutores_auxiliares/E3.txt >transdutores_auxiliares/build/E3.fsm
fsmdraw -i ../data.syms -o ../data.syms <transdutores_auxiliares/build/E1.fsm | dot -Tpdf > transdutores_auxiliares/E1.pdf
fsmdraw -i ../data.syms -o ../data.syms <transdutores_auxiliares/build/E2.fsm | dot -Tpdf > transdutores_auxiliares/E2.pdf
fsmdraw -i ../data.syms -o ../data.syms <transdutores_auxiliares/build/E3.fsm | dot -Tpdf > transdutores_auxiliares/E3.pdf
printf '\t... a criar transdutor final\n' 
#	Dezenas:

fsmconcat transdutores_auxiliares/tD.fsm transdutores_auxiliares/tU.fsm >transdutores_auxiliares/build/DU.fsm

fsmconcat transdutores_auxiliares/tD.fsm transdutores_auxiliares/build/E1.fsm >transdutores_auxiliares/build/D0.fsm

fsmunion transdutores_auxiliares/build/D0.fsm transdutores_auxiliares/build/DU.fsm >transdutores_auxiliares/build/DUvD0.fsm

fsmdraw -i ../data.syms -o ../data.syms <transdutores_auxiliares/build/DUvD0.fsm | dot -Tpdf > transdutores_auxiliares/build/dezenas.pdf

#	Centenas:

fsmconcat transdutores_auxiliares/build/E1.fsm transdutores_auxiliares/tU.fsm >transdutores_auxiliares/build/0U.fsm

fsmconcat transdutores_auxiliares/tC.fsm transdutores_auxiliares/build/DUvD0.fsm >transdutores_auxiliares/build/CD.fsm

fsmconcat transdutores_auxiliares/tC.fsm transdutores_auxiliares/build/0U.fsm >transdutores_auxiliares/build/C0U.fsm


fsmconcat transdutores_auxiliares/tC.fsm transdutores_auxiliares/build/E2.fsm >transdutores_auxiliares/build/C00.fsm

fsmunion transdutores_auxiliares/build/CD.fsm transdutores_auxiliares/build/C00.fsm >transdutores_auxiliares/build/CD\ e\ C00.fsm

fsmunion transdutores_auxiliares/build/CD.fsm transdutores_auxiliares/build/C00.fsm >transdutores_auxiliares/build/CDeC00.fsm

 fsmunion transdutores_auxiliares/build/CD.fsm transdutores_auxiliares/build/C00.fsm >transdutores_auxiliares/build/CDUC00.fsm

fsmunion transdutores_auxiliares/build/CDUC00.fsm transdutores_auxiliares/build/C0U.fsm >transdutores_auxiliares/build/centenas.fsm

fsmdraw -i ../data.syms -o ../data.syms <transdutores_auxiliares/build/centenas.fsm | dot -Tpdf > transdutores_auxiliares/build/centenas.pdf



#	Milhares:

fsmconcat transdutores_auxiliares/build/E1.fsm transdutores_auxiliares/build/DUvD0.fsm >transdutores_auxiliares/build/0D.fsm
fsmconcat transdutores_auxiliares/build/E2.fsm transdutores_auxiliares/tU.fsm >transdutores_auxiliares/build/00U.fsm 

fsmconcat transdutores_auxiliares/tM.fsm transdutores_auxiliares/build/centenas.fsm >transdutores_auxiliares/build/MC.fsm
fsmconcat transdutores_auxiliares/tM.fsm transdutores_auxiliares/build/0D.fsm >transdutores_auxiliares/build/M0D.fsm
fsmconcat transdutores_auxiliares/tM.fsm transdutores_auxiliares/build/00U.fsm >transdutores_auxiliares/build/M00U.fsm
fsmconcat transdutores_auxiliares/tM.fsm transdutores_auxiliares/build/E3.fsm >transdutores_auxiliares/build/M000.fsm

fsmunion transdutores_auxiliares/build/MC.fsm transdutores_auxiliares/build/M0D.fsm >transdutores_auxiliares/build/MCM0D.fsm
fsmunion transdutores_auxiliares/build/MCM0D.fsm transdutores_auxiliares/build/M00U.fsm >transdutores_auxiliares/build/MCM0DM00U.fsm
fsmunion transdutores_auxiliares/build/MCM0DM00U.fsm transdutores_auxiliares/build/M000.fsm >transdutores_auxiliares/build/milhares.fsm


printf '\tTransdutor romano_arabe criado\n' 

fsmunion transdutores_auxiliares/tU.fsm transdutores_auxiliares/build/DUvD0.fsm >transdutores_auxiliares/build/UvD.fsm
fsmunion transdutores_auxiliares/build/UvD.fsm transdutores_auxiliares/build/centenas.fsm >transdutores_auxiliares/build/UvDvC.fsm
fsmunion transdutores_auxiliares/build/UvDvC.fsm transdutores_auxiliares/build/milhares.fsm >romanoArabe.fsm
fsmdraw -i ../data.syms -o ../data.syms <romanoArabe.fsm | dot -Tpdf > romanoArabe.pdf
rm transdutores_auxiliares/build/*.fsm 
